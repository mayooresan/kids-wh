package com.mayuonline.kids.wnh;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

public class Report extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.report);
		
		View backgroundimage = findViewById(R.id.report);
		Drawable background = backgroundimage.getBackground();
		background.setAlpha(50);
		
		TextView txtName = (TextView)findViewById(R.id.textViewName);
		txtName.setText("Name : "+TempValues.name);
		
		TextView txtAge = (TextView)findViewById(R.id.textViewAge);
		txtAge.setText("Age : "+TempValues.age);
		
		TextView txtPS = (TextView)findViewById(R.id.textViewPS);
		txtPS.setText("This is a generalized data. You must check with your physician for more specific data for your kid. It is very normal for kids to deviate from given height and weight. ");
		
		TextView txtHeight = (TextView)findViewById(R.id.textViewHeight);
		TextView txtWeight = (TextView)findViewById(R.id.textViewWeight);
		
		if(TempValues.age.equals("Less than 3 months")){
			if(TempValues.gender.equals("Male")){
				txtHeight.setText("Height : 50.5 cm");
				txtWeight.setText("Weight : 3.3 Kg");
			}
			else{
				txtHeight.setText("Height : 49.9 cm");
				txtWeight.setText("Weight : 3.2 Kg");
			}
		}
		else if(TempValues.age.equals("3 Months")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 6 Kg");
				txtHeight.setText("Height : 66.1 cm");
			}
			else{
				txtWeight.setText("Weight : 5.4 Kg");
				txtHeight.setText("Height : 60.2 cm");
			}
		}
		
		else if(TempValues.age.equals("6 Months")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 7.8 Kg");
				txtHeight.setText("Height : 67.8 cm");
			}
			else{
				txtWeight.setText("Weight : 7.2 Kg");
				txtHeight.setText("Height : 66.6 cm");
			}
		}
		
		else if(TempValues.age.equals("9 Months")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 9.2 Kg");
				txtHeight.setText("Height : 72.3 cm");
			}
			else{
				txtWeight.setText("Weight : 8.6 Kg");
				txtHeight.setText("Height : 71.1 cm");
			}
		}
		
		else if(TempValues.age.equals("1 Year")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 10.2 Kg");
				txtHeight.setText("Height : 76.1 cm");
			}
			else{
				txtWeight.setText("Weight : 9.5 Kg");
				txtHeight.setText("Height : 75 cm");
			}
		}
		
		else if(TempValues.age.equals("2 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 12.3 Kg");
				txtHeight.setText("Height : 85.6 cm");
			}
			else{
				txtWeight.setText("Weight : 11.8 Kg");
				txtHeight.setText("Height : 84.5 cm");
			}
		}
		
		else if(TempValues.age.equals("3 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 14.6 Kg");
				txtHeight.setText("Height : 94.9 cm");
			}
			else{
				txtWeight.setText("Weight : 14.1 Kg");
				txtHeight.setText("Height : 93.9 cm");
			}
		}
		
		else if(TempValues.age.equals("4 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 16.7 Kg");
				txtHeight.setText("Height : 102.9 cm");
			}
			else{
				txtWeight.setText("Weight : 16 Kg");
				txtHeight.setText("Height : 101.6 cm");
			}
		}
		
		else if(TempValues.age.equals("5 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 18.7 Kg");
				txtHeight.setText("Height : 109.9 cm");
			}
			else{
				txtWeight.setText("Weight : 17.7 Kg");
				txtHeight.setText("Height : 108.6 cm");
			}
		}
		else if(TempValues.age.equals("6 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 20.7 Kg");
				txtHeight.setText("Height : 116.1 cm");
			}
			else{
				txtWeight.setText("Weight : 19.5 Kg");
				txtHeight.setText("Height : 114.6 cm");
			}
		}
		else if(TempValues.age.equals("7 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 22.9 Kg");
				txtHeight.setText("Height : 121.7 cm");
			}
			else{
				txtWeight.setText("Weight : 21.8 Kg");
				txtHeight.setText("Height : 120.6 cm");
			}
		}
		else if(TempValues.age.equals("8 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 25.3 Kg");
				txtHeight.setText("Height : 127.0 cm");
			}
			else{
				txtWeight.setText("Weight : 24.8 Kg");
				txtHeight.setText("Height : 126.4 cm");
			}
		}
		else if(TempValues.age.equals("9 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 28.1 Kg");
				txtHeight.setText("Height : 132.0 cm");
			}
			else{
				txtWeight.setText("Weight : 28.5 Kg");
				txtHeight.setText("Height : 132.2 cm");
			}
		}
		else if(TempValues.age.equals("10 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 31.4 Kg");
				txtHeight.setText("Height : 137.5 cm");
			}
			else{
				txtWeight.setText("Weight : 32.5 Kg");
				txtHeight.setText("Height : 138.3 cm");
			}
		}
		else if(TempValues.age.equals("11 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 32.2 Kg");
				txtHeight.setText("Height : 140.0 cm");
			}
			else{
				txtWeight.setText("Weight : 33.7 Kg");
				txtHeight.setText("Height : 142.0 cm");
			}
		}
		else if(TempValues.age.equals("12 Years")){
			if(TempValues.gender.equals("Male")){
				txtWeight.setText("Weight : 37.0 Kg");
				txtHeight.setText("Height : 147.0 cm");
			}
			else{
				txtWeight.setText("Weight : 38.7 Kg");
				txtHeight.setText("Height : 148.0 cm");
			}
		}
		
		else{
			Toast.makeText(Report.this, "Error!!!", 2000).show();
			txtWeight.setText("ERROR");
			txtHeight.setText("ERROR");
		}
	}

}
