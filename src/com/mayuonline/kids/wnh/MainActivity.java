package com.mayuonline.kids.wnh;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        
        
        View backgroundimage = findViewById(R.id.home);
        Drawable background = backgroundimage.getBackground();
        background.setAlpha(50);
        
        final Spinner spinnerAge = (Spinner)findViewById(R.id.spinnerAge);
        final Spinner spinnerGender = (Spinner)findViewById(R.id.spinnerGender);
        final EditText edtName = (EditText)findViewById(R.id.editTextName);
        
        Button btnGenerate = (Button)findViewById(R.id.buttonOK);
        btnGenerate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				TempValues.age = spinnerAge.getSelectedItem().toString();
				TempValues.gender = spinnerGender.getSelectedItem().toString();
				TempValues.name = edtName.getText().toString();
				if(TempValues.name.trim().equals("")){
					Toast.makeText(MainActivity.this, "Please enter a name", 2000).show();
				}
				else{
					//Toast.makeText(MainActivity.this, TempValues.age+TempValues.gender, 2000).show();
					Intent report = new Intent(MainActivity.this, Report.class);
					startActivity(report);
				}
				
				
			}
		});
    }

        
}
